#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include <stdbool.h>
#include "./parson.h"
enum
{
    kLength = 256,
    kElement_length = 10
};

int get_split_random(void);

int main(int argc, char* argv[])
{
    char *id[kElement_length] = {"A", "B", "C", "D", "E", "F", "G", "H", "I"};
    char *pid[kElement_length] = {"562","3601","22","831","430","2439","923","122","3638"};
    char *ip[kElement_length] = {"111.419.12.8",
                                 "210.101.2.99",
                                 "138.55.108.109",
                                 "55.215.5.5",
                                 "208.88.118.8",
                                 "222.149.9.110",
                                 "198.64.1.9",
                                 "209.118.103.121",
                                 "113.22.8.98"};
    char *emsg[kElement_length] = {"Process Succeed",
                                   "Failed Allocate to Memory",
                                   "Failed Initialize Options",
                                   "Empty InRing Buffer",
                                   "error getting local installation",
                                   "Invalid Memory Address",
                                   "Retrieved message Too big for msg",
                                   "WARNING: WaveMsg2MakeLocal rejected tracebuf",
                                   "Unknown Local Module"};
    char *etime[kElement_length] = {"2020-12-10T00:31:12",
                                    "1999-02-26T10:59:04",
                                    "2000-08-01T12:09:22",
                                    "2009-10-11T23:44:50",
                                    "1988-05-05T21:10:39",
                                    "2014-03-30T07:26:00",
                                    "2019-06-19T01:04:39",
                                    "2011-01-22T09:02:00",
                                    "2018-11-01T22:52:17"};
    char *nsc[kElement_length] = {"KS.GOCB.BGN.2016.246.00.00.00",   
                                  "KS.NAWB.LGZ.2016.246.00.00.00",
                                  "KS.YSAB.LHZ.2016.246.00.00.00",
                                  "KS.YSAB.LHE.2016.246.00.00.00",
                                  "KS.YSAB.LHN.2016.246.00.00.00",
                                  "KS.NAWB.LHE.2016.246.00.00.00",
                                  "KS.NAWB.LHN.2016.246.00.00.00",
                                  "KS.GOCB.BHE.2016.246.00.00.00",
                                  "KS.GOCB.BGZ.2016.246.00.00.00"};
    JSON_Value *root_value = NULL;
    JSON_Array *root_array = NULL;
    
    char array_key[] = "result";
    // file의 이름값 만들기.
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char currunt_date[11] = "\0";
    sprintf(currunt_date, "%04d-%02d-%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);
    char file_name[kLength] = "\0";
    strcpy(file_name, currunt_date);
    strcat(file_name, ".json");

    // 랜덤값을 반환해서 인덱스를 설정하는 구간
    int idx = get_split_random();
    // 파일이 존재하는지 찾기
    root_value = json_parse_file(file_name);
    // init_object()로 JSON_Value 를 생성하고 초기화
    if (root_value == NULL)
    {
        // root_value 초기화
        root_value = json_value_init_array();
    }
    // array 가져오기
    root_array = json_value_get_array(root_value);

    JSON_Value *array_val = json_value_init_object();
    JSON_Object *array_new_obj = json_value_get_object(array_val);

    // JSON 키:밸류 생성하는 구간
    // ecode와 emsg는 항상 일치해야 함. ecode가 곧 emsg이므로.
    json_object_set_string(array_new_obj, "ip", ip[idx]);
    json_object_set_string(array_new_obj, "id", id[idx]);
    json_object_set_string(array_new_obj, "p_id", pid[idx]);
    json_object_set_number(array_new_obj, "e_code", idx);
    json_object_set_string(array_new_obj, "e_msg", emsg[idx]);
    json_object_set_string(array_new_obj, "e_time", etime[idx]);

    json_array_append_value(root_array, array_val);

    json_serialize_to_file_pretty(root_value, file_name);
    // 해제해야 한다
    json_value_free(root_value);
    root_value = NULL;
    //usleep(1.3 * 1000000.0);
    return 0;
}

int get_split_random(void)
{
    int split_result = 0;
    int random_data = 0;
    int random_array[kElement_length] = {0, 1, 2, 3, 4, 5, 6, 7, 8};

    srand((unsigned int)time(NULL));
    random_data = rand() % 9;
    //split_result = random_array[random_data];

    return random_data;
}