#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include "./parson.h"
enum
{
    kLength = 256,
    kElement_length = 10
};

int get_split_random(void);

int main(int argc, char* argv[])
{
    char *id[kElement_length] = {"A", "B", "C", "D", "E", "F", "G", "H", "I"};
    char *pid[kElement_length] = {"562","3601","22","831","430","2439","923","122","3638"};
    char *ip[kElement_length] = {"111.419.12.8",
                                 "210.101.2.99",
                                 "138.55.108.109",
                                 "55.215.5.5",
                                 "208.88.118.8",
                                 "222.149.9.110",
                                 "198.64.1.9",
                                 "209.118.103.121",
                                 "113.22.8.98"};
    char *emsg[kElement_length] = {"Process Succeed",
                                   "Failed Allocate to Memory",
                                   "Failed Initialize Options",
                                   "Empty InRing Buffer",
                                   "error getting local installation",
                                   "Invalid Memory Address",
                                   "Retrieved message Too big for msg",
                                   "WARNING: WaveMsg2MakeLocal rejected tracebuf",
                                   "Unknown Local Module"};
    char *etime[kElement_length] = {"2020-12-10T00:31:12",
                                    "1999-02-26T10:59:04",
                                    "2000-08-01T12:09:22",
                                    "2009-10-11T23:44:50",
                                    "1988-05-05T21:10:39",
                                    "2014-03-30T07:26:00",
                                    "2019-06-19T01:04:39",
                                    "2011-01-22T09:02:00",
                                    "2018-11-01T22:52:17"};
    char filename[] = "append_write.json";
    JSON_Value *root_value = NULL;
    JSON_Object *root_object = NULL;
    int i = 0;

    while(1)
    {
        root_value = json_parse_file(filename);
        if (root_value == NULL)
        {
            root_value = json_value_init_object();
        }
        root_object = json_value_get_object(root_value);
        // 랜덤값을 반환해서 인덱스를 설정하는 구간
        int idx = get_split_random();
        // file로 저장
        char key_name[8] = "\0";
        sprintf(key_name, "%d", i);
        json_object_set_string(root_object, key_name, "deep");
        json_serialize_to_file_pretty(root_value, filename);
        ++i;
        sleep(2);
    }

    return 0;
}

int get_split_random(void)
{
    int split_result = 0;
    int random_data = 0;
    int random_array[kElement_length] = {0, 1, 2, 3, 4, 5, 6, 7, 8};

    srand((unsigned int)time(NULL));
    random_data = rand() % 9;
    //split_result = random_array[random_data];

    return random_data;
}